'use client';

import { WeatherData } from '@/lib/schemas';
import { useState } from 'react';
import styles from './ForecastSection.module.css';
import Image from 'next/image';
import { WindDirectionArrow } from './WindDirectionArrow';
import { getWindDirection } from '@/lib/utils';
import { WeekContent } from './WeekForecast';
import { DayContent } from './DayForecast';

export function ForecastSection(weatherData: WeatherData) {
  const [activeTab, setActiveTab] = useState<'week' | 'day'>('week');

  const handleTabChange = (tab: 'week' | 'day') => {
    setActiveTab(tab);
  };

  return (
    <section className={styles.container}>
      <div className={styles.weekDayToggle}>
        <button
          className={activeTab === 'week' ? styles.activeDay : styles.inActive}
          onClick={() => handleTabChange('week')}
        >
          Week
        </button>
        <button
          className={activeTab === 'day' ? styles.activeWeek : styles.inActive}
          onClick={() => handleTabChange('day')}
        >
          Day
        </button>
      </div>

      <div>
        {activeTab === 'week' && <WeekContent {...weatherData} />}
        {activeTab === 'day' && <DayContent {...weatherData} />}
      </div>
    </section>
  );
}
