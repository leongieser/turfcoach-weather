'use client';
import styles from './SearchBar.module.css';
import { useRouter } from 'next/navigation';
import { Icons } from '@/icons';
import { useTheme } from 'next-themes';
import { useMounted, useGeolocation } from '@/hooks';
import { toast } from 'sonner';

export const SearchBar = () => {
  const router = useRouter();
  const { theme, setTheme } = useTheme();
  const mounted = useMounted();

  if (!mounted) return null;

  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const formData = new FormData(event.currentTarget);
    const city = formData.get('city') as string;
    if (!city) return;
    router.push(`/?city=${city}`);
  };

  const handleThemeChange = () => {
    setTheme(theme === 'dark' ? 'light' : 'dark');
  };

  const handleGeolocation = async () => {
    toast.warning('Geolocation has not been implemented yet.');
  };

  return (
    <div className={styles.container}>
      <button onClick={handleThemeChange} type="button">
        {theme === 'dark' ? (
          <Icons.sun className={styles.darkmodeicon} />
        ) : (
          <Icons.moon className={styles.darkmodeicon} />
        )}
      </button>

      <form onSubmit={handleSubmit} className={styles.form}>
        <input
          className={styles.search}
          type="search"
          name="city"
          required
          placeholder="Search..."
        />
        <button className={styles.searchbutton} type="submit">
          <Icons.magnifyingGlass className={styles.searchicon} />
        </button>
      </form>
      <button onClick={handleGeolocation} type="button">
        <Icons.geopin className={styles.icon} />
      </button>
    </div>
  );
};
