import { Icons } from '@/icons';
import { WeatherData } from '@/lib/schemas';
import { getWindDirection } from '@/lib/utils';
import styles from './WindDirectionArrow.module.css';

export const WindDirectionArrow = ({
  windDirection,
}: {
  windDirection: string;
}) => {
  switch (windDirection) {
    case 'North':
      return <Icons.North className={styles.windDirectionIcon} />;
    case 'North-East':
      return <Icons.NorthEast className={styles.windDirectionIcon} />;
    case 'East':
      return <Icons.East className={styles.windDirectionIcon} />;
    case 'South-East':
      return <Icons.SouthEast className={styles.windDirectionIcon} />;
    case 'South':
      return <Icons.South className={styles.windDirectionIcon} />;
    case 'South-West':
      return <Icons.SouthWest className={styles.windDirectionIcon} />;
    case 'West':
      return <Icons.West className={styles.windDirectionIcon} />;
    case 'North-West':
      return <Icons.North className={styles.windDirectionIcon} />;
    default:
      return <Icons.North className={styles.windDirectionIcon} />;
  }
};
