'use client';

import { useRouter } from 'next/navigation';

import styles from './CityPreview.module.css';
import { RealTimeWeather } from '@/lib/schemas';

import Image from 'next/image';

export const CityPreview = ({ cities }: { cities: RealTimeWeather[] }) => {
  return (
    <section className={styles.cityPreviewContainer}>
      {cities.map((city, idx) => {
        return <PreviewCard key={idx} city={city} />;
      })}
    </section>
  );
};

const PreviewCard = ({ city }: { city: RealTimeWeather }) => {
  const router = useRouter();
  const onClick = () => {
    router.push(`/?city=${city.timezone?.split('/').at(-1)}`);
  };
  const cityName = city.timezone?.split('/').at(-1);
  const imageUrl = `/${cityName?.toLowerCase()}.jpeg`;

  return (
    <div onClick={onClick} className={styles.cardContainer}>
      <div className={styles.imageContainer}>
        <Image
          src={imageUrl}
          alt="img of berlin"
          className={styles.cityImage}
          width={300}
          height={100}
        />
      </div>
      <div className={styles.detailsContainer}>
        <div className={styles.detailsWrapper}>
          <div className={styles.detailsText}>
            <h2 className={styles.detailsTextCity}>{cityName}</h2>
            <p className={styles.detailsTextWeatherType}>
              {city.current.weather.at(0)?.main}
            </p>
          </div>
        </div>
        <Image
          className={styles.weatherIcon}
          src={`https://openweathermap.org/img/wn/${
            city.current.weather.at(0)?.icon
          }@2x.png`}
          alt={'weather icon'}
          width={75}
          height={50}
        />

        <p className={styles.deatilsTemperature}>
          {city.current.temp?.toFixed(0)}°C
        </p>
      </div>
    </div>
  );
};
