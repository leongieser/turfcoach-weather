import { WeatherData } from '@/lib/schemas';
import { getWindDirection } from '@/lib/utils';
import Image from 'next/image';
import styles from './DayForecast.module.css';
import { WindDirectionArrow } from './WindDirectionArrow';
import { Icons } from '@/icons';

export const DayContent = (weatherData: WeatherData) => {
  return (
    <div>
      {weatherData.hourly?.map((hour, idx) => {
        const windDirection = getWindDirection(hour.wind_deg);

        return (
          <div className={styles.dayPreviewCard} key={idx}>
            <div>
              <Image
                className={styles.weatherIcon}
                src={`https://openweathermap.org/img/wn/${
                  hour.weather.at(0)?.icon
                }@2x.png`}
                alt={'weather icon'}
                width={75}
                height={75}
              />
            </div>
            <div className={styles.data}>
              <div className={styles.contentLeft}>
                {hour.wind_speed?.toFixed(0)} km/h
                <WindDirectionArrow windDirection={windDirection} />
              </div>

              <div className={styles.contentCenter}>
                {hour.temp?.toFixed(0)} °C
              </div>
              <div className={styles.contentRight}>
                Hu: {hour.humidity?.toFixed(0)}
              </div>
              <div className={styles.contentRight}>
                Uv: {hour.uvi?.toFixed(0)}
              </div>
            </div>
            <div className={styles.timeStamp}>
              <Icons.clock className={styles.clockIcon} />
              {new Date(hour.dt! * 1000).getHours()}
            </div>
          </div>
        );
      })}
    </div>
  );
};
