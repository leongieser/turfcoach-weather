import { WeatherData } from '@/lib/schemas';
import styles from './CityDetailView.module.css';
import Image from 'next/image';
import { ForecastSection } from './ForecastSection';
import { WindDirectionArrow } from './WindDirectionArrow';
import { getWindDirection } from '@/lib/utils';

export function CityDetailView(weatherData: WeatherData) {
  const windDirection = getWindDirection(weatherData.current.wind_deg);
  return (
    <>
      <section className={styles.container}>
        <div className={styles.topSection}>
          <div className={styles.left}>
            <div className={styles.nameAndTemp}>
              <h2>{weatherData.timezone?.split('/').at(-1)}</h2>
              <h3>{weatherData.current.temp?.toFixed(0)} °C </h3>
            </div>
            <h4>feels like {weatherData.current.feels_like?.toFixed(0)}</h4>
            <div className={styles.minMaxTempWrapper}>
              <p> min {weatherData.daily[0].temp.min?.toFixed(0)}</p>
              <span>/</span>
              <p> max {weatherData.daily[0].temp.min?.toFixed(0)} °C</p>
            </div>

            <div className={styles.windContainer}>
              <div className={styles.titleAndDirectionArrow}>
                <h3>Wind</h3>
                <WindDirectionArrow windDirection={windDirection} />
              </div>
              <p>Speed: {weatherData.current.wind_speed?.toFixed(0)} km/h</p>
              <p>Gust: {weatherData.current.wind_speed?.toFixed(0)} km/h</p>
            </div>
          </div>

          <div className={styles.right}>
            <div className={styles.descriptionAndUVIndex}>
              <h3>{weatherData.current.weather.at(0)?.description}</h3>
              <h4>
                Visibility:{' '}
                {((weatherData.current.visibility || 0) / 1000).toFixed(1)} km
              </h4>
              <h4>UV index: {weatherData.current.uvi}</h4>
              <h4>Humidity: {weatherData.current.humidity}%</h4>
            </div>
            <div className={styles.weatherIconWrapper}>
              <Image
                className={styles.weatherIcon}
                src={`https://openweathermap.org/img/wn/${
                  weatherData.current.weather.at(0)?.icon
                }@2x.png`}
                alt={'weather icon'}
                width={75}
                height={75}
              />
            </div>
          </div>
        </div>
      </section>

      <ForecastSection {...weatherData} />
    </>
  );
}
