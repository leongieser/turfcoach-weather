import { WeatherData } from '@/lib/schemas';
import { getWindDirection } from '@/lib/utils';
import Image from 'next/image';
import styles from './WeekForecast.module.css';
import { WindDirectionArrow } from './WindDirectionArrow';

export const WeekContent = (weatherData: WeatherData) => {
  return (
    <div>
      {weatherData.daily.slice(0, 7).map((day, idx) => {
        const windDirection = getWindDirection(day.wind_deg);
        return (
          <div className={styles.weekPreviewCard} key={idx}>
            <div>
              <Image
                className={styles.weatherIcon}
                src={`https://openweathermap.org/img/wn/${
                  day.weather.at(0)?.icon
                }@2x.png`}
                alt={'weather icon'}
                width={75}
                height={75}
              />
            </div>
            <div className={styles.data}>
              <div className={styles.contentLeft}>
                {day.wind_speed?.toFixed(0)} km/h
                <WindDirectionArrow windDirection={windDirection} />
              </div>
              <div className={styles.contentCenter}>
                {day.temp.min?.toFixed(0)} / {day.temp.max?.toFixed(0)} °C
              </div>

              <div className={styles.contentRight}>
                Hu: {day.humidity?.toFixed(0)}
              </div>
              <div className={styles.contentRight}>
                Uv: {day.uvi?.toFixed(0)}
              </div>
            </div>

            <div className={styles.DayOfTheWeek}>
              {new Date(day.dt * 1000).toLocaleDateString(undefined, {
                weekday: 'short',
              })}
            </div>
          </div>
        );
      })}
    </div>
  );
};
