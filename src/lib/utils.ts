import { GEOCODING_URL } from '@/constants';
import { GeoLocationSchema } from './schemas';
import { WEATHER_URL } from '@/constants';

export async function convertCityToGeolocation(cityName: string) {
  const response = await fetch(
    `${GEOCODING_URL}?q=${cityName}&limit=1&appid=${process.env.OPENWEATHER_API_KEY}`
  );

  if (!response.ok) {
    throw new Error('Failed to convert city to geolocation');
  }

  const data = await response.json();

  const parsedData = GeoLocationSchema.safeParse(data);

  if (!parsedData.success) {
    throw new Error('Failed to parse geolocation data');
  }

  return parsedData.data[0];
}

export function constructUrl(
  lat: number,
  lon: number,
  units: string,
  exlcude?: string[]
) {
  const excludeString = `&exclude=${exlcude ? exlcude.join(',') : ''}`;

  return `${WEATHER_URL}?lat=${lat}&lon=${lon}${excludeString}&units=${units}&appid=${process.env.OPENWEATHER_API_KEY}`;
}

export function getWindDirection(degrees: number | undefined): string {
  if (!degrees) return 'N/A';
  const directions = [
    'North',
    'NorthEast',
    'East',
    'SouthEast',
    'South',
    'SouthWest',
    'West',
    'NorthWest',
  ];
  const index =
    Math.round(((degrees %= 360) < 0 ? degrees + 360 : degrees) / 45) % 8;
  return directions[index];
}
