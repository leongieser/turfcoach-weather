import { z } from 'zod';

export const RealTimeWeatherSchema = z.object({
  lat: z.number(),
  lon: z.number(),
  timezone: z.string().optional(),
  timezone_offset: z.number().optional(),
  current: z.object({
    dt: z.number().optional(),
    sunrise: z.number().optional(),
    sunset: z.number().optional(),
    temp: z.number().optional(),
    feels_like: z.number().optional(),
    pressure: z.number().optional(),
    humidity: z.number().optional(),
    dew_point: z.number().optional(),
    uvi: z.number().optional(),
    clouds: z.number().optional(),
    visibility: z.number().optional(),
    wind_speed: z.number().optional(),
    wind_deg: z.number().optional(),
    wind_gust: z.number().optional(),
    weather: z.array(
      z.object({
        id: z.number().optional(),
        main: z.string().optional(),
        description: z.string().optional(),
        icon: z.string().optional(),
      })
    ),
  }),
});

export type RealTimeWeather = z.infer<typeof RealTimeWeatherSchema>;
