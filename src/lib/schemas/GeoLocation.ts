import { z } from 'zod';

export const GeoLocationSchema = z.array(
  z.object({
    name: z.string(),
    lat: z.number(),
    lon: z.number(),
    country: z.string(),
  })
);

export type GeoLocation = z.infer<typeof GeoLocationSchema>;
