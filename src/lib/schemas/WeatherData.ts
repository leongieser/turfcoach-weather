import { z } from 'zod';

/**
 * Turns out OpenWeather is super annoying and doesn't have a consistent API response.
 * @see https://openweathermap.org/api/one-call-3#:~:text=%22tags%22%3A%20%5B%0A%0A%20%20%20%20%20%20%5D%0A%20%20%20%20%7D%2C%0A%20%20%20%20...%0A%20%20%5D-,Fields%20in%20API%20response,-If%20you%20do
 **/

export const WeatherDataSchema = z.object({
  lat: z.number().optional().optional(),
  lon: z.number().optional(),
  timezone: z.string().optional(),
  timezone_offset: z.number().optional(),
  current: z.object({
    dt: z.number(),
    sunrise: z.number().optional(),
    sunset: z.number().optional(),
    temp: z.number().optional(),
    feels_like: z.number().optional(),
    pressure: z.number().optional(),
    humidity: z.number().optional(),
    dew_point: z.number().optional(),
    uvi: z.number().optional(),
    clouds: z.number().optional(),
    visibility: z.number().optional(),
    wind_speed: z.number().optional(),
    wind_deg: z.number().optional(),
    wind_gust: z.number().optional(),
    weather: z.array(
      z.object({
        id: z.number().optional(),
        main: z.string().optional(),
        description: z.string().optional(),
        icon: z.string().optional(),
      })
    ),
  }),
  minutely: z.array(
    z.object({
      dt: z.number().optional(),
      precipitation: z.number().optional(),
    })
  ),
  hourly: z
    .array(
      z.object({
        dt: z.number().optional(),
        temp: z.number().optional(),
        feels_like: z.number().optional(),
        pressure: z.number().optional(),
        humidity: z.number().optional(),
        dew_point: z.number().optional(),
        uvi: z.number().optional().optional(),
        clouds: z.number().optional(),
        visibility: z.number().optional(),
        wind_speed: z.number().optional(),
        wind_deg: z.number().optional(),
        wind_gust: z.number().optional(),
        weather: z.array(
          z
            .object({
              id: z.number().optional(),
              main: z.string().optional(),
              description: z.string().optional(),
              icon: z.string().optional(),
            })
            .optional()
        ),
        pop: z.number().optional(),
      })
    )
    .optional(),
  daily: z.array(
    z.object({
      dt: z.number(),
      sunrise: z.number(),
      sunset: z.number(),
      moonrise: z.number(),
      moonset: z.number(),
      moon_phase: z.number(),
      summary: z.string(),
      temp: z.object({
        day: z.number().optional(),
        min: z.number().optional(),
        max: z.number().optional(),
        night: z.number().optional(),
        eve: z.number().optional(),
        morn: z.number().optional(),
      }),
      feels_like: z.object({
        day: z.number().optional(),
        night: z.number().optional(),
        eve: z.number().optional(),
        morn: z.number().optional(),
      }),
      pressure: z.number().optional(),
      humidity: z.number().optional(),
      dew_point: z.number().optional(),
      wind_speed: z.number().optional(),
      wind_deg: z.number().optional(),
      wind_gust: z.number().optional(),
      weather: z.array(
        z.object({
          id: z.number().optional(),
          main: z.string().optional(),
          description: z.string().optional(),
          icon: z.string().optional(),
        })
      ),
      clouds: z.number().optional(),
      pop: z.number().optional(),
      rain: z.number().optional(),
      uvi: z.number().optional(),
    })
  ),
  alerts: z
    .array(
      z
        .object({
          sender_name: z.string().optional(),
          event: z.string().optional(),
          start: z.number().optional(),
          end: z.number().optional(),
          description: z.string().optional(),
          tags: z.array(z.string()).optional(),
        })
        .optional()
    )
    .optional(),
});

export type WeatherData = z.infer<typeof WeatherDataSchema>;
