import Link from 'next/link';
import Image from 'next/image';
import styles from './NotFound.module.css';

export default function NotFound() {
  return (
    <div className={styles.container}>
      <h2>Not Found</h2>
      <Image
        src="/sad_sun.png"
        alt="image of a sad cloud"
        width={500}
        height={500}
      />

      <Link href="/">Return Home</Link>
    </div>
  );
}
