'use client';
import Image from 'next/image';
import styles from './error.module.css';
import { useEffect } from 'react';

export default function Error({
  error,
  reset,
}: {
  error: Error & { digest?: string };
  reset: () => void;
}) {
  useEffect(() => {
    console.error(error);
  }, [error]);

  return (
    <div className={styles.container}>
      <h2>Something went wrong!</h2>
      <Image
        src="/sad_cloud.png"
        alt="image of a sad cloud"
        width={500}
        height={500}
      />
      <button onClick={() => reset()}>Try again</button>
    </div>
  );
}
