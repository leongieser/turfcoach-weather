import styles from './page.module.css';
import { getRealTimeWeather, getWeatherData } from '@/actions';
import { CityPreview, SearchBar, CityDetailView } from '@/components';

export default async function Home({
  searchParams,
}: {
  searchParams: { city: string };
}) {
  const { city } = searchParams;

  if (!city) {
    const majorCities = await Promise.all([
      getRealTimeWeather('Berlin'),
      getRealTimeWeather('Paris'),
      getRealTimeWeather('London'),
      getRealTimeWeather('Tokyo'),
    ]);

    return (
      <>
        <main className={styles.main}>
          <h1 className={styles.heroheader}>Wheater or not</h1>
          <SearchBar />
          <CityPreview cities={majorCities} />
        </main>
      </>
    );
  }

  const weatherData = await getWeatherData(city);
  return (
    <main className={styles.mainDetail}>
      <h1 className={styles.heroheader}>Wheater or not</h1>
      <SearchBar />
      <CityDetailView {...weatherData} />
    </main>
  );
}
