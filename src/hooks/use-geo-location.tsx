import { useState, useEffect } from 'react';

interface GeolocationData {
  latitude: number | null;
  longitude: number | null;
  error: string | null;
}

export function useGeolocation(): GeolocationData {
  const [geolocationData, setGeolocationData] = useState<GeolocationData>({
    latitude: null,
    longitude: null,
    error: null,
  });

  useEffect(() => {
    setGeolocationData({
      latitude: null,
      longitude: null,
      error: 'Geolocation is not implemented yet.',
    });
  }, []);

  return geolocationData;
}
