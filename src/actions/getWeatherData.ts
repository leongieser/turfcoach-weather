import { convertCityToGeolocation, constructUrl } from '@/lib/utils';
import { WeatherDataSchema } from '@/lib/schemas';
import { z } from 'zod';
import { info } from 'console';
import { Herr_Von_Muellerhoff } from 'next/font/google';

/**
 * @comment
 * I made my inital plans with the google places api in mind to get the image and geolocation data for the city.
 * Google did not accept my payment method so I had to use a different api for the geolocation data.
 * Which lead down a rabbit hole of improvisation.
 * I was unable to find a good source for images in time so I had to opt for hardcoding the landing page images.
 * Of course now i remember that I could have used the unsplash api for the images.
 * This would have been the place to attach the image data to the weather data.
 */

export async function getWeatherData(
  location: string,
  units: string = 'metric'
) {
  const { lat, lon } = await convertCityToGeolocation(location);

  const url = constructUrl(lat, lon, units, []);

  const response = await fetch(url, {
    next: {
      revalidate: 10 * 60 * 1000,
    },
  });

  if (!response.ok) {
    throw new Error('Failed to fetch weather data');
  }

  const data = await response.json();

  try {
    const parsedData = WeatherDataSchema.parse(data);

    return parsedData;
  } catch (error) {
    console.log(error);
    throw new Error('Failed to parse weather data');
  }
}
