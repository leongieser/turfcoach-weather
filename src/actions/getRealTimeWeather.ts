import { convertCityToGeolocation, constructUrl } from '@/lib/utils';
import { RealTimeWeatherSchema } from '@/lib/schemas';

export async function getRealTimeWeather(
  location: string,
  units: string = 'metric'
) {
  const { lat, lon } = await convertCityToGeolocation(location);
  const url = constructUrl(lat, lon, units, [
    'minutely',
    'hourly',
    'daily',
    'alerts',
  ]);

  const response = await fetch(url, {
    next: {
      revalidate: 10000,
    },
  });

  if (!response.ok) {
    throw new Error('Failed to fetch realtime weather data');
  }

  const data = await response.json();

  try {
    const parsedData = RealTimeWeatherSchema.parse(data);
    return parsedData;
  } catch (error) {
    console.log(error);
    throw new Error('Failed to parse realtime weather data');
  }
}
