export const WEATHER_URL = 'https://api.openweathermap.org/data/3.0/onecall';
export const GEOCODING_URL = 'http://api.openweathermap.org/geo/1.0/direct';

// https://openweathermap.org/weather-conditions
export const WEATHER_CODE_MAP: Record<string, { desc: string; icon: string }> =
  {
    // Thunderstorm
    '200': { desc: 'thunderstorm with light rain', icon: '11d' },
    '201': { desc: 'thunderstorm with rain', icon: '11d' },
    '202': { desc: 'thunderstorm with heavy rain', icon: '11d' },
    '210': { desc: 'light thunderstorm', icon: '11d' },
    '211': { desc: 'thunderstorm', icon: '11d' },
    '212': { desc: 'heavy thunderstorm', icon: '11d' },
    '221': { desc: 'ragged thunderstorm', icon: '11d' },
    '230': { desc: 'thunderstorm with light drizzle', icon: '11d' },
    '231': { desc: 'thunderstorm with drizzle', icon: '11d' },
    '232': { desc: 'thunderstorm with heavy drizzle', icon: '11d' },
    // Drizzle
    '300': { desc: 'light intensity drizzle', icon: '09d' },
    '301': { desc: 'drizzle', icon: '09d' },
    '302': { desc: 'heavy intensity drizzle', icon: '09d' },
    '310': { desc: 'light intensity drizzle rain', icon: '09d' },
    '311': { desc: 'drizzle rain', icon: '09d' },
    '312': { desc: 'heavy intensity drizzle rain', icon: '09d' },
    '313': { desc: 'shower rain and drizzle', icon: '09d' },
    '314': { desc: 'heavy shower rain and drizzle', icon: '09d' },
    '321': { desc: 'shower drizzle', icon: '09d' },
    // Rain
    '500': { desc: 'light rain', icon: '10d' },
    '501': { desc: 'moderate rain', icon: '10d' },
    '502': { desc: 'heavy intensity rain', icon: '10d' },
    '503': { desc: 'very heavy rain', icon: '10d' },
    '504': { desc: 'extreme rain', icon: '10d' },
    '511': { desc: 'freezing rain', icon: '13d' },
    '520': { desc: 'light intensity shower rain', icon: '09d' },
    '521': { desc: 'shower rain', icon: '09d' },
    '522': { desc: 'heavy intensity shower rain', icon: '09d' },
    '531': { desc: 'ragged shower rain', icon: '09d' },
    // Snow
    '600': { desc: 'light snow', icon: '13d' },
    '601': { desc: 'snow', icon: '13d' },
    '602': { desc: 'heavy snow', icon: '13d' },
    '611': { desc: 'sleet', icon: '13d' },
    '612': { desc: 'light shower sleet', icon: '13d' },
    '613': { desc: 'shower sleet', icon: '13d' },
    '615': { desc: 'light rain and snow', icon: '13d' },
    '616': { desc: 'rain and snow', icon: '13d' },
    '620': { desc: 'light shower snow', icon: '13d' },
    '621': { desc: 'shower snow', icon: '13d' },
    '622': { desc: 'heavy shower snow', icon: '13d' },
    // Atmosphere
    '701': { desc: 'mist', icon: '50d' },
    '711': { desc: 'smoke', icon: '50d' },
    '721': { desc: 'haze', icon: '50d' },
    '731': { desc: 'sand/dust whirls', icon: '50d' },
    '741': { desc: 'fog', icon: '50d' },
    '751': { desc: 'sand', icon: '50d' },
    '761': { desc: 'dust', icon: '50d' },
    '762': { desc: 'volcanic ash', icon: '50d' },
    '771': { desc: 'squalls', icon: '50d' },
    '781': { desc: 'tornado', icon: '50d' },
    // Clear
    '800': { desc: 'clear sky', icon: '01d' },
    // Clouds
    '801': { desc: 'few clouds: 11-25%', icon: '02d' },
    '802': { desc: 'scattered clouds: 25-50%', icon: '03d' },
    '803': { desc: 'broken clouds: 51-84%', icon: '04d' },
    '804': { desc: 'overcast clouds: 85-100%', icon: '04d' },
  };
