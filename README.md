[Live version](https://turfcoach-weather.vercel.app/)

This app is using the [one-call-3.0 openweather api](https://openweathermap.org/api/one-call-3)
This is the paid startup plan.

If you want to run it the app locally feel free to use this api key: "ZERO 8d3c26dfcd5 ZERO c8 ZERO 65e1f32c95f2 ZERO 372"
I will deactivate end of the week.

copy the .env.sample file paste the key and rename it to .env.local

run `npm i` from your shell (min Node 18.17 required) then run `npm run dev` and visit http://localhost:3000/
